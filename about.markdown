---
layout: page
title: About
permalink: /about/
---

## This is real
This resource is a notebook - pieces of study notes and logs.
Totally  DIY made with `Git + Jekyll`.

Hi, **Kostya** is here.

I am tireless seeker of knowledge. I am a network engineer by training.
Occasionally, I am a network developer.


## Certifications
- Cisco CCNA
- Cisco DevNet Associate
- CompTIA
    - A+
    - Network+
    - Security+
    - Project+
    - Linux+
- LPI 1 (Linux Certification)



>  I truly believe that ***skies is the limit***.




![a balloon in the skies](/balloon.jpg)
