---
layout: post
title: "Sticky bit and other advanced permissions"
categories: Linux
---
### This is kinda complicated

Let's make it easy.

Some time you see:

![ls output ](/img/20-04-25-1.png)

Do you see big T? Do you know why? Isn't it beyond of standard `rwx` rights?

Or sometime you see:

![ls output ](/img/20-04-25-2.png)

Aren't there two big `S` and small `t`?

I know, it's complicated.

What does it mean?

### SUID  - set user ID

You know about `x` - it means execution. Right? If you have `x` right you
can execute this file or if  it's directory you can list files in it.

If a file or script has the **set user ID (SUID)** bit/permission enabled, any user who executes
the file, executes that file with permissions of the owner.
```
chmod u+s files

```
When you change your password, you execute `/usr/bin/passwd`.
The `passwd` needs access to `/etc/shadow` file which owned by root and **no-one** except root
can read or change it.

To allow users change their passwords, `passwd` has SUID bit enabled. User receives
root permission to write in `shadow` file. And SUID looks like `s` in place of `x`
```bash
[root@ server1 ~]# ls -l /usr/ bin/ passwd
-rwsr-xr-x. 1 root root 32680 Apr 2t 20120 /usr/ bin/ passwd

```
But sometime you see capitalized  `S`. It mens `x` bit/permission is *not* enabled.

```bash
[root@ server1 ~]# ls -l file
-rwSr-xr-x. 1 root root 32680 Apr 2t 20:20  file

```
### SGID - set group ID

You can enable set group ID permission

```
chmod g+s directory/

```

If applied to a directory,
 all files and subdirectories  in that directory created by anyone
get the same group ownership.
```
[root@ server1 /]# ls -ld  share.d/
drwxr-sr-x. 2 root share 4096 Apr 25 20:25 share.d/
[root@ server1 /share.d]# touch file.txt
[root@ server1 /share.d]# ls -l
-rwxrw-r--. 2 root share 4096 Apr 25 20:29 file.txt

```
### Sticky bit

And why it's `sticky`?

Basicly sticky bit makes files stick around. No one can delete a file with
the sticky bit enabled *(except the owner of the file or directory owner )*

For example, there are bunch of files:

![Text file listing in terminal](/img/20-04-25-3.png)

Let's make one of them *stick*

![Text file listing in console](/img/20-04-25-4.png)

File gets capital T.

Let's add executable permission and see what happens:

![Text file listing in console](/img/20-04-25-5.png)

It's now an executable file with small t.

Let's enable set group ID bit on all of the txt files.

![ listing  of text fiels in console](/img/20-04-25-6.png)

Set group ID is shown as  `s` on 1.txt because `x` bit is enabled.
And in other files as capital `S` because `x` executable bit is disabled.
Here it goes!
