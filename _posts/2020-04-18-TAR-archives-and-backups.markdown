---
layout: post
title: "TAR command"
categories: Linux
---
### TAR is good for your soul

TAR - tape archiver.
No type - no problem. You can ***tar*** your files on any type of storage.

Let's create an archive: *Dash is optional*
```
tar -cfv my_archive.tar  some_files/*
```
Here is some arguments which would simplify your archiving:
 `-c` create

 `-f` archive file to create or  use `-` to send to standard output.

 `-v` verbose

 `--one-file-system`  gets data only from current file system
 `--exclude=` - obviously you want exclude redundant stuff, don't you.



 `-r` to append files to an existing archive

 And when your archive created a while ago, but a lot of files changed:
 `-u` -  (for updates) is going to help
```
tar -uvf  existing_archive updated_files
```
 What is in that archive? -t
```
 tar -tvf archive.tar
```
 How to extract files: -x
```
tar -xvf archive.tar
```
 By default files extracted in the current directory. -C helps to extract fies
 to the target directory.
```
tar -xvf archive -C mydir/subdir/
```
### Add some compression

 Usually when archiving files we compress them, so it take less space to store
 and bandwidth. `-z ` is for gunzip; `-j` for bunzip.
```
 tar -cvfz
```
### Preserving right
Aren't we obsessed with our rights? Keep in mind tar easily violates permissions
a.k.a rights when used improperly. When you extract an archive with other users files
they become your files.

So please use `sudo` to create and extract archives.

### Archiving files around

If archiving files in known directory was a problem, it would never be a problem.
The real problem is to archive a lot f files in a lot of directories and bringing
them together in one place to create an archive. Would be it a challenge?

Nope!

There is a well know and reliable `find` which does exactly that: it finds files.
It's a powerful tool. Man page for find is a book itself.

So

First, we are looking for some_files
```
find some_directory/ -name "*.txt"
```
Good. Wee found them all text files in some directory. How to archive them?

```
find  \  -iname "*.txt" -exec tar -rvjf files.tgz  {} \;
```

We loooking for some files and adding them to compressed  files archive.
Very important  - it's not `-c`! There is `-r` You want to append found some_files
to the archive.

Using `-c` would give files.tgz with just one file - found last.
