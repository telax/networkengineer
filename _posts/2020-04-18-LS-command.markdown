---
layout: post
title: "LS command"
categories: Linux
---


Show recursively descendants of all subdirectories
```
ls -R  
```
Show files sorted on modification date - recently modified last in the listed
```
ls -lrt
```
