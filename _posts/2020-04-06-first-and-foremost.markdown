---
layout: post
title:  "First and foremost"
date:   2020-04-16 19:58:40 -0700
categories: Linux
---

Who doesn't like to build staff? Who doesn't get excited about new things?
What about combining these two together? What about creating new stuff on *your* own?

Creating, building, learning by doing...
