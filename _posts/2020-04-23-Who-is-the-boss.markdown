---
layout: post
title: "Root | Who is the boss?"
categories: Linux
---
## Root
Any Unix like OS hast 2 types of users: privileged and not privileged.
Default privileged user is called root.

It lets you become the root and work in root's environment - if you know the password.
Obviously, too much power corrupts and makes you dangerous.
One root's typing error potentially is a security risk escpecially in graphical environment.

## SU

Any user in the system could become the root.

```bash
su -

```
Or any user for that matter:

```bash

su trump

```

## SUDO

Another way to run privileged commands, is SUDO

### Enable sudo access.

To make a single user sudoer: add a file with following line to the `/etc/sudoers.d/ `
directory

```bash
 username ALL=(ALL:ALL)  ALL

 ```
User `username` can run privileged commands after entering its password.

How about group?

```bash

  %username ALL=(ALL:ALL)  ALL

 ```

### Sudo without password

```bash
  username ALL=(ALL:ALL) NOPASSWD: ALL

```
```bash
  %username ALL=(ALL:ALL) NOPASSWD: ALL

```

It's very important to verify correct syntax of all sudoers files:

```bash

visudo -c

```
## Another way

Add the user  `username` to the `wheel`

```bash

 usermod -aG wheel username

```
Make sure that group `wheel`  in  in  `visudo` is included and not commented.

```bash

%wheel  ALL=(ALL:ALL) ALL

```
