---
layout: post
title: "Passwords and shadows"
categories: Linux
---
## USERS
One who uss a computer need some kind of account. Account is a lot of things.
It's a container - username, password, user directory, applications, groups and so on.
Obviously a computer system tracks and manages users and level of access to the system.

There are 2 main files which contain user information `/etc/passwd` and `/etc/shadow`.
Yep,  **shadow**. What kind of shadws?

## Shadows

/etc/shadow contains one record (one line) for each user, as in:

shadow contains a line per a user  a colon-separated fields

- *username*: unique user name
- *password*: the hashed (sha512) value of the password
- *lastchange*: days since Jan 1,1970 that password was last changed
- *mindays*: minimum days before password can be changed
- *maxdays*: maximum days after which password must be changed
- *warn*: days before password expires that the user is warned
- *grace*: days after password expires that account is disabled
- *expire*: date that account is/will be disabled
- *reserved*: reserved field.

Yep, there is some ***grace*** for everybody.
More important it contains the holy grail -- passwords. Beware! but not in clear
text, password are hashed. So it makes to protect this file because there is a
possibility to extract passwords with  things like rainbow attack or
different kinds of rippers.

So this is why /etc/shadow has 400 permission ``(-r--------)`` only root can read
this file.



## Passwd

user:x:1001:1001:Awesome Dude:/home/user:/bin/bash

A line per user with colon-separated fields which are:

- *username* - the user's unique name
- *password* - either the hashed password (if /etc/shadow is not used) or a placeholder ("x" when /etc/shadow is used)
- *UID* - user identification number
- *GID* - primary group identification number for the user
- *comment* - comment area, usually the user's real name
- *home* - directory pathname for the user's home directory
- *shell* - absolutely qualified name of the shell to invoke at login.

## Password Management
```bash
  passwd
```
This is the command to change user's password.


## Age restrictions

Unix system is age restricted  or discriminatory. ***Age discrimination*** here.
Yes, against password. And it probably a good practice to change password sometime.
Not too often.

```
chage
  [-m mindays]
  [-M maxdays]
  [-d lastday]
  [-I inactive]
  [-E expiredate]
  [-W warndays]
                user
```

Want someone suffer? Make his password expire yesterday.

```bash
 chage -E 1970-01-01 user

 ```

Want to make the change password?

```bash
 chage -d 0 user

 ```
