---
layout: post
title: "CP command"
categories: Linux
---

How to copy files in linux
```
 cp
```
How to recursively copy files in a directories. It might be confusing, `cp`
doesn't touch your subdirectories. What if you want to copy files, subdirectories,
and it's content? Recursion, man!
```
cp -R
```
And don't forget about **permissions**. Does cp cares about files properties like permissions?
Not much. So how to copy files properties? Make `cp` work in archive mode.
```
cp -a
```
And be careful. ***cp*** doesn't care about dot files! Because they are dot files
for a reason. They are ***hidden***.
So...
To copy .files you can:
```
cp from/.* to/
```
But it doesn't copy hidden subdirectories. Add -R
```
cp -R from/.* to/
```
Or
```
cp -a from/. to/
```
would copy ***everything***, man, everything
