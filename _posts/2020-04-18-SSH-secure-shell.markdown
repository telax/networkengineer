---
layout: post
title: "SSH is here for you"
categories: Linux
---
### Secure access to other machines

No one `likes` passwords. End there is way to access remote machine without entering
your credentials ever again. How?

```
 user@machine1 $ ssh-keygen  # generates key pare

 user@machine1 $ssh-copy-id -i .ssh/id_rsa.pub # copies public key to a remote machine
```

Repeat the same on the second machine
```
 user@machine2 $ ssh-keygen  # generates key pare

 user@machine2 $ ssh-copy-id -i .ssh/id_rsa.pub # copies public key to a remote machine
```
And magic happens - ain't passwords asked:
```
user@machine2 $ ssh user@machine1
user@machine1 $
```

## X over SSH

Don't you want a nice GUI over SSH

Add the following line to **/etc/ssh/ssh_config**
```
 ForwardX11 yes
```
Restart sshd
```
systemctl restart ssh
```
And then log into remote machine:
```
ssh -X user@machine1
```
And if everything is right you might be lucky enough to run a graphic application.
