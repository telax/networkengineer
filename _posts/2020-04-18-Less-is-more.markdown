---
layout: post
title: "Less is more"
categories: Linux
---
### in UNIX less is almost more

Two utilities with similar functions: **less** and **more**

A long time ago there was **more** and it was good.
And there were some who wanted more. Didn't the already have more? The more features, the better.

So UNIX god's provided their users with less - actually **less** has more feauture,
but it doeesn't matter any more.
If you want more, use Less
> less big_long_file.txt
> more big_long_file.txt

It's better than **cat**, even some like cats more.

> cat long_file

Of course there's **tail** if you want look at the end.
And **head** if you care only about the beginning.

> head -n 5 big_long_file #show 5 first lines
> tail -n 5 big_long_file #show 5 last lines
