---
layout: post
title: "Bash and Scripting Basics"
categories: Linux
---
## Do Not repeat yourself
When in shell - save time don't be like me - use the following *shortcuts*
instead of retyping last command or arguments:
```
    !$  last argument from previous command
    !^  first argument (after the program/built-in/script) from previous command
    !!  previous command (often pronounced "bang bang")
    !n  command number n from history
    !pattern  most recent command matching pattern
    !!:s/find/replace last command, substitute find with replace
```
Don't forget about `Alt` + `.` (Press alt and . simultaneously) which would scroll through all your historical arguments.

Your shell scripts have access to the following environmental variables:
```
    $0 - The name of the Bash script.
    $1 - $9 - The first 9 arguments to the Bash script. (As mentioned above.)
    $# - How many arguments were passed to the Bash script.
    $@ - All the arguments supplied to the Bash script.
    $? - The exit status of the most recently run process.
    $$ - The process ID of the current script.
    $USER - The username of the user running the script.
    $HOSTNAME - The hostname of the machine the script is running on.
    $SECONDS - The number of seconds since the script was started.
    $RANDOM - Returns a different random number each time is it referred to.
    $LINENO - Returns the current line number in the Bash script.
```
