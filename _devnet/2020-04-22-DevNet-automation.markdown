---
layout: post
title: "DevNet - Network automation"
date:   2020-04-22 11:09:40 -0700
categories: DevNet
---
## REST API responses



REST is everything and everywhere. You click on a link in any webbroser - REST wokrs behind.
Smartphone apps - REST is there, REST is web is http.

When you send a request, you get response:
* HTTP Status
* Header
* Body

HTTP Status
* 1xx Info
* 2xx Success /request accepted/
* 3xx Redirection /additional action required or auto redirection
* 4xx Client Error /client's request contains an error (remember 404)
* 5xx Server Error /request valid, but server side error prevents responses

HTTP Status Code |	Status Message |	Description
200 |	OK |	Request was successfully and typically contains a payload (body)
201 |	Created |	Request was fulfilled and the requested resource was created
202 |	Accepted |	Request has been accepted for processing but is not complete
400 |	Bad Request |	Request will not be processed due to an error with the request
401 |	Unauthorized |	Request does not have valid authentication credentials to perform the request
403 |	Forbidden |	Request was understood but has been rejected by the server
404 |	Not Found |	Request cannot be fulfilled because the resource path of the request was not found on the server
500 |	Internal Server Error |	Request cannot be fulfilled due to a server error
503 |	Service Unavailable |	Request cannot be fulfilled because currently the server cannot handle the request
