---
layout: post
title: "DevNet - Rest API Examples"
date:   2020-04-23 20:09:40 -0700
categories: DevNet

---
```python
import requests
import json
from pprint import pprint
```


```python
url = "https://api.icndb.com/jokes/random?limitTo=nerdy"

c = requests.get(url)

```


```python
joke = c.json()
print(joke["value"]['joke'])
```

    Chuck Norris solved the halting problem.



```python
router = {"ip": "ios-xe-mgmt.cisco.com",
	      "port": "9443",
          "user": "developer",
          "pass": "C1sco12345"}

headers = {"Accept": "application/yang-data+json"}

u = "https://{}:{}/restconf/data/interfaces/interface=GigabitEthernet2"

u = u.format(router["ip"], router["port"])

r = requests.get(u,
                 headers = headers,
                 auth=(router["user"], router["pass"]),
                 verify=False)

#pprint(r.text)

api_data = r.json()
interface_name = api_data["Cisco-IOS-XE-interfaces-oper:interface"]["ipv4"]
print(interface_name)
```
    10.255.255.1

```python

```
